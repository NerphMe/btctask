<?php

namespace App\Controller;

use App\Entity\BTC;
use App\Form\BTCType;
use App\Repository\BTCRepository;
use http\Env\Request;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;

class RateController extends AbstractController
{

    /**
     * @Route("/test")
     */
    public function LoadFileToDb()
    {
        $file = file_get_contents('/Users/macbook/Desktop/KriptTask/task/task.json');
        $data = json_decode($file, true);

        foreach ($data as $key => $value) {

            $new = new BTC();
            $new->setCurrencyPair($key);
            $new->setCurrencyRate($value);
            $em = $this->getDoctrine()->getManager();
            $em->persist($new);
            $em->flush();
        }


        return new \Symfony\Component\HttpFoundation\Response();


    }

    /**
     * @Route("/all", name="all")
     */
    public function getData()
    {

        $em = $this->getDoctrine()->getManager();
        $btc = $em->getRepository(BTC::class)->findAll();
        return $this->render('all.html.twig', ['btcs' => $btc]);

    }


    /**
     * @Route("/addData", name="addData")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addData(\Symfony\Component\HttpFoundation\Request $request)
    {
        $addData = new BTC();
        $form = $this->createForm(BTCType::class, $addData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addData = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($addData);
            $em->flush();
        }

        return $this->render('addData.html.twig', [
            'form' => $form->createView()
        ]);


    }

}