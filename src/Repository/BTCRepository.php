<?php

namespace App\Repository;

use App\Entity\BTC;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BTC|null find($id, $lockMode = null, $lockVersion = null)
 * @method BTC|null findOneBy(array $criteria, array $orderBy = null)
 * @method BTC[]    findAll()
 * @method BTC[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BTCRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BTC::class);
    }

    // /**
    //  * @return BTC[] Returns an array of BTC objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BTC
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
