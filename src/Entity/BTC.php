<?php

namespace App\Entity;

use App\Repository\BTCRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BTCRepository::class)
 */
class BTC
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currencyPair;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $currencyRate;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCurrencyPair()
    {
        return $this->currencyPair;
    }

    /**
     * @param mixed $currencyPair
     */
    public function setCurrencyPair($currencyPair): void
    {
        $this->currencyPair = $currencyPair;
    }

    /**
     * @return mixed
     */
    public function getCurrencyRate()
    {
        return $this->currencyRate;
    }

    /**
     * @param mixed $currencyRate
     */
    public function setCurrencyRate($currencyRate): void
    {
        $this->currencyRate = $currencyRate;
    }


}
